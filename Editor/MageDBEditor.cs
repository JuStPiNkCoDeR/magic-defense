﻿using System;
using System.Collections;
using System.Collections.Generic;
using DB;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MageDB))]
public class MageDBEditor : Editor
{
    private MageDB db;

    private void Awake()
    {
        db = (MageDB) target;
    }

    public override void OnInspectorGUI()
    {
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("RemoveAll"))
            db.ClearDB();
        if (GUILayout.Button("Remove"))
            db.RemoveCurrent();
        if (GUILayout.Button("Add"))
            db.Add();
        if (GUILayout.Button("=>"))
            db.GetNext();
        if (GUILayout.Button("<="))
            db.GetPrev();
        GUILayout.EndHorizontal();
        base.OnInspectorGUI();
    }
}