﻿using Game;

namespace Interfaces
{
    public interface IPoolAble
    {
        //Активация объекта
        void Activate();
        //Дезактивация объекта
        void Disable();
    }
}