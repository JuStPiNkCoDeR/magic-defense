﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class Swiper : MonoBehaviour
{
    private Vector2 _startPos;
    private Vector2 _direction;
    private bool _directionChosen;
    private Directions _dir = Directions.Nothing;

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _startPos = touch.position;
                    _directionChosen = false;
                    break;

                case TouchPhase.Moved:
                    _direction = touch.position - _startPos;
                    break;

                case TouchPhase.Ended:
                    _directionChosen = true;
                    break;
            }

            if (_directionChosen)
            {
                if (Math.Abs(_direction.x) - Math.Abs(_direction.y) < Math.Abs(_direction.y) - Math.Abs(_direction.x))
                {
                    if (_direction.x < 0)
                    {
                        _dir = Directions.Left;
                    }
                    else
                    {
                        _dir = Directions.Right;
                    }
                }
                else
                {
                    if (_direction.y > 0)
                    {
                        _dir = Directions.Up;
                    }
                    else
                    {
                        _dir = Directions.Nothing;
                    }
                }
            }
        }
    }

    public Directions GetDirection()
    {
        return _dir;
    }
}
