﻿using System;
using UnityEngine;

namespace Game
{
    public class Movement
    {
        // configs
        private Vector3[] _chosenWay;

        public Vector3[] ChosenWay
        {
            get => _chosenWay;
        }

        private int[] _indexPath = new int[2];
        private bool? _isGoingToUp = null;
        private float _speed = 10f;
        
        // controls
        private int _pointCounter = 0;
        private int? _needPointsToGo = null;
        private int _currentIndex;
        private bool _isGoingAvailable = false;

        public bool IsGoingAvailable => _isGoingAvailable;

        public Movement(Vector3[] path, int startIndex, int finishIndex)
        {
            _chosenWay = path;
            SetIndexes(startIndex, finishIndex);
        }
        
        public Movement(Vector3[] path, int startIndex, int finishIndex, float speed)
        {
            _chosenWay = path;
            SetIndexes(startIndex, finishIndex);
            _speed = speed;
        }

        public void SetIndexes(int start, int end)
        {
            _indexPath[0] = start;
            _indexPath[1] = end;

            _isGoingToUp = start < end;
            _needPointsToGo = start - end < 0 ? -(start - end) : start - end;

            if (_needPointsToGo > 0)
                _isGoingAvailable = true;
        }

        public void Proceed(GameObject obj)
        {
            if (_chosenWay == null || _isGoingToUp == null)
            {
                _isGoingAvailable = false;
                return;
            }

            if (!(_pointCounter <= _needPointsToGo))
            {
                _isGoingAvailable = false;
                return;
            }
            
            var xCord = _chosenWay[_indexPath[0]].x;
            var yCord = _chosenWay[_indexPath[0]].y;

            var position = obj.transform.position;
            var distToPoint = GetDistToPoint(xCord, yCord, position);
            var xStep = (xCord - position.x) / distToPoint;
            var yStep = (yCord - position.y) / distToPoint;
                    
            obj.transform.Translate(xStep * Time.deltaTime*_speed, yStep * Time.deltaTime*_speed, 0f);
            if (distToPoint < 0.25f)
            {
                _pointCounter++;
                if (_isGoingToUp != null && (bool) _isGoingToUp) _indexPath[0]++;
                else _indexPath[0]--;
            }
        }

        private float GetDistToPoint(float xCord, float yCord, Vector3 position)
        {
            var distToPoint =
                Mathf.Sqrt(Mathf.Pow((xCord - position.x), 2f) + Mathf.Pow((yCord - position.y), 2f));
            return distToPoint;
        }
    }
}