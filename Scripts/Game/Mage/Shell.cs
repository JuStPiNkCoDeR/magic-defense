﻿using System;
using Data;
using Game;
using Interfaces;
using UnityEngine;
using Movement = Game.Movement;
using Vector3 = UnityEngine.Vector3;

public class Shell : MonoBehaviour, IPoolAble
{
    private Vector3 _initialPosition;
    private Movement _moveControl;
    private Vector3[] _path;
    private bool _isActive = false;
    private SpriteRenderer _spriteRenderer;
    
    public delegate void AtTouchWithEnemy();

    public AtTouchWithEnemy onTouch; 

    public string Colour { get; set; }

    public ObjectsPool<Shell> Pool { get; set; }

    public Vector3[] Path
    {
        get => _path;
        set
        {
            _path = value;
            _moveControl = new Movement(value, 0, _path.Length - 1);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(transform.position);
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isActive) return;
        
        if (_moveControl.IsGoingAvailable)
            _moveControl.Proceed(gameObject);
        else
        {
            Pool.ReturnToPool(this);
            return;
        }
        if (!gameObject.GetComponent<Renderer>().isVisible)
            Pool.ReturnToPool(this);
    }
    
    public void Activate()
    {
        _isActive = true;    
    }

    public void Disable()
    {
        GameObject obj;
        (obj = gameObject).SetActive(false);
        this._isActive = false;
        obj.transform.position = _initialPosition;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!_isActive) return;
        var enemy = other.gameObject.GetComponent<Enemy>();
        
        if (enemy == null) return;
        enemy.Pool.ReturnToPool(enemy);
        onTouch();
        Pool.ReturnToPool(this);
    }
}
