﻿using System;
using UnityEngine;

namespace Game
{
    public class SwipeControl
    {
        private bool _swiping = false;
        private bool _eventSent = false;
        private Vector2 _lastPosition;

        public Directions Check()
        {
            if (Input.touchCount == 0) return Directions.Nothing;

            var ans = Directions.Nothing;
            if (Input.GetTouch(0).deltaPosition.sqrMagnitude != 0f)
            {
                if (!_swiping)
                {
                    _swiping = true;
                    _lastPosition = Input.GetTouch(0).position;
                    return Directions.Nothing;
                }
                else
                {
                    if (_eventSent) return ans;
                    var direction = Input.GetTouch(0).position - _lastPosition;
                    var dist = Mathf.Sqrt(Mathf.Pow(direction.x, 2f) + Mathf.Pow(direction.y, 2f));

                    if (dist < 1f) return ans;

                    if (Math.Abs(direction.x) > Math.Abs(direction.y))
                    {
                        ans = direction.x > 0 ? Directions.Right : Directions.Left;
                    }
                    else
                    {
                        ans = direction.y > 0 ? Directions.Up : Directions.Down;
                    }

                    _eventSent = true;
                }
            }
            else
            {
                _swiping = false;
                _eventSent = false;
            }

            return ans;
        }
    }
}