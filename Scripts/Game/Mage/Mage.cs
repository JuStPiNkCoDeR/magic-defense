﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

/* 
    @Author -> JuStPiNkCoDeR (Alexander Los)
    @Description -> Class which control mage activities
*/
namespace Game
{
    public class Mage : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
    {
        public MagicSpawner MagicSpawner;

        private readonly SwipeControl _swiper = new SwipeControl();
        private void Start()
        {
            
        }
        private void Update()
        {
            var swipeHandler = _swiper.Check();
            if (swipeHandler == Directions.Nothing) return;
            
            SimpleShoot(swipeHandler);
        }

        private void SimpleShoot(Directions dir)
        {
            MagicSpawner.Shoot(dir);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            /*var array = new[] {Directions.Left, Directions.Up, Directions.Right};
            var idx = (int) Random.value * array.Length;
            SimpleShoot(array[idx]);*/
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            
        }
    }
}
