﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace Game
{
    public class ManaBar : MonoBehaviour
    {
        private const float Max = 1f;
        private float _currentMana;
        private float _timeLeftToRefreshMana = 0f;
        private float _timeLeftToGainMana = 0f;
        public float timePerGainMana;
        public float timeToBeginRefreshMana;
        public float gainManaPerFrame;

        public float CurrentMana
        {
            get => _currentMana;
        }

        private Transform _bar;
        // Start is called before the first frame update
        void Start()
        {
            _bar = transform.Find("Bar");
            _currentMana = 1f;
        }

        // Update is called once per frame
        void Update()
        {
            if (_timeLeftToRefreshMana > 0f)
            {
                _timeLeftToRefreshMana -= Time.deltaTime;
            }
            else
            {
                if (!(_currentMana < Max)) return;
                if (_timeLeftToGainMana > 0f)
                    _timeLeftToGainMana -= Time.deltaTime;
                else
                {
                    UpdateAmountOfMana(gainManaPerFrame);
                    _timeLeftToGainMana = timePerGainMana;
                }
            }
        }

        public void SubtractMana(float amount)
        {
            _timeLeftToGainMana = timePerGainMana;
            _timeLeftToRefreshMana = timeToBeginRefreshMana;
            UpdateAmountOfMana(-amount);
        }

        private void UpdateAmountOfMana(float add)
        {
            _currentMana += add;
            UpdateAmountOfMana();
        }

        private void UpdateAmountOfMana()
        {
            _bar.localScale = new Vector3(_currentMana, 1f);
        }
    }
}
