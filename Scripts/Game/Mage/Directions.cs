﻿namespace Game
{
    public enum Directions
    {
        Up,
        Right,
        Down,
        Left,
        Nothing
    }
}