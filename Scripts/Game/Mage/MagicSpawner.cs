﻿using Data.Controls;
using UnityEngine;

namespace Game
{
    public class MagicSpawner : MonoBehaviour
    {
        public Shell shell;
        public float speed;
        public int maxAmountOfShells;
        public Data.Movement movementSet;
        public ScoreCounter scoreCounter;

        private ObjectsPool<Shell> _shellsPool;
        private MovementControl _mvControl;
        private int _amountOfActiveShells = 0;
        private Shell _currentBullet;

        // Start is called before the first frame update
        void Start()
        {
            shell.gameObject.SetActive(false);
            Debug.Log(gameObject.transform.position);
            _shellsPool = new ObjectsPool<Shell>(maxAmountOfShells, () => Instantiate(shell, transform.position, Quaternion.Euler(new Vector3(0, 0, 0))));
            _mvControl = new MovementControl(movementSet);
            ShowCurrentBullet();
        }
    
        public void Shoot(Directions dir) {
            if (dir == Directions.Down) return;
        
            _shellsPool.ActivateNextObject(obj =>
            {
                obj.Path = _mvControl.GetPoints(_mvControl.GetWay(dir));
                obj.Pool = _shellsPool;
                obj.onTouch = () => { scoreCounter.AddScore(5, Colours.Blue); };
            });
            ShowCurrentBullet();
            _amountOfActiveShells++;
        }

        private void ShowCurrentBullet()
        {
            _currentBullet = _shellsPool.ShowNextObject();
            _currentBullet.gameObject.SetActive(true);
        }
    }
}
