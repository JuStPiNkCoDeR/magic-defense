﻿using System;
using System.Collections;
using System.Collections.Generic;
using DB;
using Interfaces;
using JetBrains.Annotations;
using UnityEngine;

namespace Game
{
    /*
     * Автор: JuStPiNkCoDeR
     * Класс для пулов
     */
    public class ObjectsPool<T> where T: IPoolAble
    {
        //Объекты пула должны наследовать IPoolAble интерфейс
        private readonly Queue<T> _pool = new Queue<T>();

        //Размер очереди
        public int MaxSize { get; }
        
        //Функция вызываемая при добавлении в пул
        public delegate T AddToPool();

        //Функция вызываемая при активации объекта
        public delegate void OnActivate(T obj);

        //По сути безполезный конструктор
        public ObjectsPool(int size)
        {
            MaxSize = size;
        }
        
        /* Первый аргумент размер очереди
         * Второй аргумент это функция для добавления в очередь
         *
         * Функция создаст очередь нужного размера, используя данную функцию*/
        public ObjectsPool(int size, AddToPool func)
        {
            MaxSize = size;
            for (var i = 0; i < size; i++)
                _pool.Enqueue(func());
        }

        //Просмотреть следующий объект без его взятия
        public T ShowNextObject()
        {
            return _pool.Peek();
        }
        
        //Взять из очереди следующий объект и посмотреть его
        public T TakeNextObject()
        {
            return _pool.Dequeue();
        }

        /*Взять из очереди следующий объект и активировать его
          Аргументом передается функция с доп инструкциями при активации объекта*/
        public void ActivateNextObject(OnActivate func)
        {
            if (_pool.Count == 0)
            {
                Debug.Log("Пул пуст!");
                return;
            }
            
            var obj = _pool.Dequeue();
            func(obj);
            obj.Activate();
        }

        //Возвращение в очередь
        public void ReturnToPool(T obj)
        {
            obj.Disable();
            _pool.Enqueue(obj);
        }
    }
}
