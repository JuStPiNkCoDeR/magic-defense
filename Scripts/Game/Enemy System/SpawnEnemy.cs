﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Game;
using Interfaces;

public class SpawnEnemy : MonoBehaviour
{
    public int maxAmountOfEnemies;
    public EnemySet enemySet;
    public LineRenderer way;
    public float timeDelay = 2f;

    private float timer;
    private ObjectsPool<Enemy> _enemyPool;
    public Vector3[] _wayPoints;

    void Start()
    {
        _wayPoints = new Vector3[way.positionCount];
        way.GetPositions(_wayPoints);
        timer = timeDelay;
        _enemyPool = new ObjectsPool<Enemy>(maxAmountOfEnemies,() => Instantiate(enemySet.GetEnemy(0), this.transform));
    }

    void Update()
    {
        if (timer <= 0)
        {
            if (!_enemyPool.ShowNextObject().DoesActive())
            {
                _enemyPool.ActivateNextObject(obj =>
                {
                    obj.Path = _wayPoints;
                    obj.Pool = _enemyPool;
                });
            }
            timer = timeDelay;
        }
        else timer -= Time.deltaTime;
    }
}
