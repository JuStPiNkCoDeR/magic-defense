﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;
using Interfaces;

public class Enemy : MonoBehaviour, IPoolAble
{
    public float speed = 3f;

    private Vector3 _initialPosition;
    private Movement _moveControl;
    private Vector3[] _path;
    private int _cost = 3;
    
    public bool isActive = false;

    public int Cost => _cost;

    public ObjectsPool<Enemy> Pool { get; set; }

    public Vector3[] Path
    {
        get => _path;
        set
        {
            _path = value;
            _moveControl = new Movement(value, _path.Length-1, 0, speed);
        }
    }

    void Start()
    {
        _initialPosition = transform.position;
    }

    void Update()
    {
        if (!isActive) return;
        //Debug.Log("Объект активирован");
        if (_moveControl.IsGoingAvailable) { 
            //Debug.Log("Объект движется");
            _moveControl.Proceed(gameObject);
        }
        else
        {
            Pool.ReturnToPool(this);
            return;
        }
    }

    public void Activate()
    {
        gameObject.SetActive(true);
        isActive = true;

    }

    public void Disable()
    {
        gameObject.SetActive(false);
        gameObject.transform.position = _initialPosition;
        isActive = false;
    }

    public bool DoesActive()
    {
        return isActive;
    }
}
