﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    /*private EnemyData enemyData;

    private Vector3[] points;

    private LineRenderer currentLine;
    public int pointCounter = 0;

    private float xPointCoordinate;
    public float yPointCoordinate;

    public float xDirection;
    public float yDirection;
    private float distEnemyToPoint;
    void Start()
    {
        enemyData = this.GetComponentInParent<CurrentEnemyData>().GetEnemyData();

        currentLine = GetComponentInParent<SpawnEnemy>().GetWay();
        points = new Vector3[currentLine.positionCount];
        currentLine.GetPositions(points);

        xPointCoordinate = points[pointCounter].x;
        yPointCoordinate = points[pointCounter].y;

        distEnemyToPoint = Mathf.Sqrt(Mathf.Pow((xPointCoordinate - transform.position.x), 2f) + Mathf.Pow((yPointCoordinate - transform.position.y), 2f));

        xDirection = (xPointCoordinate - transform.position.x) / distEnemyToPoint;
        yDirection = (yPointCoordinate - transform.position.y) / distEnemyToPoint;
    }

    void Update()
    {
        if (pointCounter < points.Length && DistEnemyToPoint() < 0.05f)
        {
            pointCounter++;
            if (pointCounter >= points.Length)
            {
                GetComponentInParent<CurrentEnemyData>().SetActive(false);
                GetComponentInParent<SpawnEnemy>().PushInPool(this.gameObject);
                Destroy(transform);
                return;
            }
            xPointCoordinate = points[pointCounter].x;
            yPointCoordinate = points[pointCounter].y;
            
            distEnemyToPoint = Mathf.Sqrt(Mathf.Pow((xPointCoordinate - transform.position.x), 2f) + Mathf.Pow((yPointCoordinate - transform.position.y), 2f));

            xDirection = (xPointCoordinate - transform.position.x) / distEnemyToPoint;
            yDirection = (yPointCoordinate - transform.position.y) / distEnemyToPoint;
        }

        if (pointCounter < points.Length)
        {
            transform.Translate(xDirection * enemyData.GetSpeed() * Time.deltaTime, yDirection * enemyData.GetSpeed() * Time.deltaTime, 0);
        }
    }

    float DistEnemyToPoint()
    {
        float distEnemyToPointInFunc = Mathf.Sqrt(Mathf.Pow((xPointCoordinate - transform.position.x), 2f) + Mathf.Pow((yPointCoordinate - transform.position.y), 2f));
        return distEnemyToPointInFunc;
    }

    public void SetWay(LineRenderer way)
    {
        currentLine = way;
    }*/
}
