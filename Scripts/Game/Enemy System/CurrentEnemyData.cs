﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentEnemyData : MonoBehaviour
{
    public EnemyData enemyData;
    [SerializeField]
    private bool _inActive = false;
    private float _currentHealth;
    private float _currentSpeed;
    private Renderer renderer;
    private LineRenderer _currentWay;
    private byte _typeID;

    public void Awake()
    {
        //_currentWay = this.GetComponentInParent<SpawnEnemy>().GetWay();
        renderer = GetComponent<Renderer>();
        SetVisible(false);
        _currentHealth = enemyData.GetHealth();
        _currentSpeed = enemyData.GetSpeed();
        _typeID = enemyData.GetTypeID();
    }

    public bool DoesInActive()
    {
        return _inActive;
    }

    public void SetActive(bool active)
    {
        _inActive = active;
    }

    public EnemyData GetEnemyData()
    {
        return enemyData;
    }

    public void SetVisible(bool visible)
    {
        renderer.enabled = visible;
    }

    public float GetSpeed()
    {
        return _currentSpeed;
    }

    public byte GetTypeID()
    {
        return _typeID;
    }
}
