﻿using System;
using Data;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class ScoreCounter: MonoBehaviour
    {
        public Text text;
        private int _blueScore;
        private int _redScore;
        private int _greenScore;
        private const string BlueScoreText = "Blue score : ";
        private const string RedScoreText = "Red score : ";
        private const string GreenScoreText = "Green score : ";

        private void Awake()
        {
            _blueScore = 0;
            _redScore = 0;
            _greenScore = 0;
            ShowText();
        }

        public void AddScore(int newScore, Colours cl)
        {
            switch (cl)
            {
                case Colours.Blue:
                    _blueScore += newScore;
                    break;
                case Colours.Green:
                    _greenScore += newScore;
                    break;
                case Colours.Red:
                    _redScore += newScore;
                    break;
                default:
                    _blueScore += newScore / 3;
                    _redScore += newScore / 3;
                    _greenScore += newScore / 3;
                    break;
            }
            
            ShowText();   
        }

        private void ShowText()
        {
            text.text = BlueScoreText + _blueScore + "\n" +
                        RedScoreText + _redScore + "\n" +
                        GreenScoreText + _greenScore;
        }
    }
}