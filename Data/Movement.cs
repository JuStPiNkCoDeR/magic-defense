﻿using UnityEngine;
namespace Data
{
    [CreateAssetMenu(menuName = "DB/Simple Movement", fileName = "New Movement Set")]
    public class Movement: ScriptableObject
    {
        [SerializeField, Tooltip("Левая траектория")]
        public LineRenderer leftWay;

        [SerializeField, Tooltip("Средняя траектория")]
        public LineRenderer midWay;

        [SerializeField, Tooltip("Правая траектория")]
        public LineRenderer rightWay;
    }
}