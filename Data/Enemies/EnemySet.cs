﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "DB/Enemy Set", fileName = "New Enemy Set")]
public class EnemySet : ScriptableObject
{
    [SerializeField]
    private Enemy[] _enemyTypes = new Enemy[1];

    public Enemy GetEnemy(int x)
    {
        return _enemyTypes[x];
    }

    public int GetSetLength()
    {
        return _enemyTypes.Length;
    }
}
