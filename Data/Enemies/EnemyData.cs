﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DB/Enemy", fileName = "New Enemy")]
public class EnemyData : ScriptableObject
{
    [SerializeField]
    private float _health = 100f;

    [SerializeField]
    private float _speed = 0.1f;

    [SerializeField]
    private byte typeID;
    public float GetSpeed()
    {
        return _speed;
    }

    public float GetHealth()
    {
        return _health;
    }

    public byte GetTypeID()
    {
        return typeID;
    }
}
