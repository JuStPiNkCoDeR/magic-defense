﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageDataAD : ScriptableObject
{
    [SerializeField]
    private GameObject[] _spellsSet;
    [SerializeField]
    private byte _sphereID;

    public byte GetSphereID()
    {
        return _sphereID;
    }
}
