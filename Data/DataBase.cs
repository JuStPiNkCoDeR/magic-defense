﻿using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "DB/Simple DB", fileName = "New Database")]
    public class DataBase<T>: ScriptableObject where T : ScriptableObject
    {
        [SerializeField] private List<T> dataList;

        [SerializeField] private T currentData;

        private int _currentIndex = 0;

        public T this[int index]
        {
            get
            {
                if (index >= 0 && index < dataList.Count && dataList != null)
                    return dataList[index];
                return null;
            }
            set
            {
                if (dataList == null)
                    dataList = new List<T>();

                if (index >= 0 && index < dataList.Count && value != null)
                {
                    dataList[index] = value;
                }
            }
        }
        public void Add()
        {
            if (dataList == null)
                dataList = new List<T>();

            currentData = ScriptableObject.CreateInstance<T>();
            dataList.Add(currentData);
            _currentIndex = dataList.Count - 1;
        }

        public void RemoveCurrent()
        {
            if (_currentIndex > 0)
            {
                currentData = dataList[--_currentIndex];
                dataList.RemoveAt(++_currentIndex);
            }
            else
            {
                dataList.Clear();
                currentData = null;
            }
        }
    }
}