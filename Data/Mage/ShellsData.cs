﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DB/Shell", fileName = "Shell")]
public class ShellsData : ScriptableObject
{
    [SerializeField] public Sprite blueSprite;
    [SerializeField] public Sprite greenSprite;
    [SerializeField] public Sprite redSprite;

    public Sprite GetSprite(string spriteName)
    {
        Sprite ans = null; 
        
        switch (spriteName)
        {
            case "blue":
                ans = blueSprite;
                break;
            case "green":
                ans = greenSprite;
                break;
            case "red": 
                ans = redSprite;
                break;
        }
        
        return ans;
    }
}
