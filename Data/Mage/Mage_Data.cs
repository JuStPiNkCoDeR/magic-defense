﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


[CreateAssetMenu(menuName = "DB/Mage", fileName = "New Mage")]
public class MageData: ScriptableObject
{
    private const float maxMana = 100f;
    public float MaxMana => maxMana * maxManaMultiplier;

    private const float simpleShootManaCost = 0.1f;
    public float SimpleShootManaCost => simpleShootManaCost * simpleShootManaCostMultiplier;

    private const float gainManaPerOneMoment = 0.05f;
    public float GainManaPerOneMoment => gainManaPerOneMoment * gainManaPerMomentMultiplier;
    
    private const float timePerGainMana = 0.05f;
    public float TimePerGainMana => timePerGainMana * timePerGainManaMultiplier;

    private const float timeToBeginRefreshMana = 1f;
    public float TimeToBeginRefreshMana => timeToBeginRefreshMana * timeToBeginRefreshManaMultiplier;
    
    [SerializeField, Tooltip("Множитель для количества маны (100)")]
    public float maxManaMultiplier;
    public float MaxManaMultiplier
    {
        get => maxManaMultiplier;
        set => maxManaMultiplier = value;
    }

    [SerializeField, Tooltip("Float number greater than 0 which will multiply simple mana cost")]
    public float simpleShootManaCostMultiplier;
    public float SimpleShootManaCostMultiplier
    {
        get => simpleShootManaCostMultiplier;
        set => simpleShootManaCostMultiplier = value;
    }

    [SerializeField, Tooltip("Множитель для количество получаемой самособой маны за один момент")]
    public float gainManaPerMomentMultiplier;

    public float GainManaPerMomentMultiplier
    {
        get => gainManaPerMomentMultiplier;
        set => gainManaPerMomentMultiplier = value;
    }

    [SerializeField, Tooltip("Множитель для задержки между одним моментом получения маны")]
    public float timePerGainManaMultiplier;

    public float TimePerGainManaMultiplier
    {
        get => timePerGainManaMultiplier;
        set => timePerGainManaMultiplier = value;
    }

    [SerializeField, Tooltip("Множитель для задержки к началу процесса самовостановления маны")]
    public float timeToBeginRefreshManaMultiplier;
    public float TimeToBeginRefreshManaMultiplier
    {
        get => timeToBeginRefreshManaMultiplier;
        set => timeToBeginRefreshManaMultiplier = value;
    }
}