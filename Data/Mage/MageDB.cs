﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

namespace DB
{
    [CreateAssetMenu(menuName = "DB/Mages", fileName = "Mages")]
    public class MageDB : ScriptableObject
    {
        [SerializeField] private List<MageData> magesList;

        [SerializeField] private MageData currentMage;

        private int _currentIndex = 0;

        public void Add()
        {
            if (magesList == null)
                magesList = new List<MageData>();
      
            currentMage = ScriptableObject.CreateInstance<MageData>();
            magesList.Add(currentMage);
            _currentIndex = magesList.Count - 1;
        }

        public void RemoveCurrent()
        {
            if (_currentIndex > 0)
            {
                currentMage = magesList[--_currentIndex];
                magesList.RemoveAt(++_currentIndex);
            }
            else
            {
                magesList.Clear();
                currentMage = null;
            }
        }

        public MageData GetNext()
        {
            if (_currentIndex < magesList.Count - 1)
                _currentIndex++;
            currentMage = this[_currentIndex];
            return currentMage;
        }
   
        public MageData GetPrev()
        {
            if (_currentIndex > 0)
                _currentIndex--;
            currentMage = this[_currentIndex];
            return currentMage;
        }

        public void ClearDB()
        {
            magesList.Clear();
            magesList.Add(ScriptableObject.CreateInstance<MageData>());
            _currentIndex = 0;
            currentMage = magesList[_currentIndex];
        }

        public MageData this[int index]
        {
            get
            {
                if (magesList != null && index >= 0 && index < magesList.Count)
                    return magesList[index];
                return null;
            }
            set
            {
                if (magesList == null)
                    magesList = new List<MageData>();

                if (index >= 0 && index < magesList.Count && value != null)
                    magesList[index] = value;
                else Debug.LogError("Выход за границы списка магов");
            }
        }
    }
}
