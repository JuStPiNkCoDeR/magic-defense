﻿using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "DB/Main Info", fileName = "Information")]
    public class GameInfo: ScriptableObject
    {
        [field: SerializeField]
        public int Score { set; get; }

        public void RefreshData()
        {
            Score = 0;
        }
    }
}