﻿using Game;
using UnityEngine;

namespace Data.Controls
{
    public class MovementControl
    {
        private readonly Movement _movementSet;

        public MovementControl(Movement movementSet)
        {
            this._movementSet = movementSet;
        }

        public LineRenderer GetWay(Directions dir)
        {
            switch (dir)
            {
                case Directions.Left:
                    return _movementSet.leftWay;
                case Directions.Up:
                    return _movementSet.midWay;
                case Directions.Right:
                    return _movementSet.rightWay;
                default:
                    return null;
            }
        }

        public Vector3[] GetPoints(LineRenderer path)
        {
            var points = new Vector3[path.positionCount];
            path.GetPositions(points);
            return points;
        }
    }
}